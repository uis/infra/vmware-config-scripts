# Configure NTP correctly for a VM host.

$rightntps = "ntp0.cam.ac.uk", "ntp1.cam.ac.uk", "ntp2.cam.ac.uk", "ntp3.cam.ac.uk"

function Configure-NTP1 {
    param($VMHost)
    $changed = $false
    $ntps = Get-VMHostNtpServer $VMHost
    foreach ($ntp in $ntps) {
        if ($ntp -notIn $rightntps) {
	    Remove-VMHostNtpServer -VMHost $VMHost -NtpServer $ntp
	    $changed = $true
	}
    }
    foreach ($ntp in $rightntps) {
        if ($ntp -notIn $ntps) {
	    Add-VMHostNtpServer -VMHost $VMHost -NtpServer $ntp
	    $changed = $true
	}
    }
    $ntpd = Get-VMHostService -VMHost $VMHost | where key -eq "ntpd"
    if ($ntpd.policy -ne "on") {
	Set-VMHostService $ntpd -policy on
	$changed = $true
    }
    if ($changed) {
	if ($ntpd.running) {
	    Restart-VMHostService $ntpd
	} else {
	    Start-VMHostService $ntpd
	}
    }
}
