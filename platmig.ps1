#! /usr/bin/env pwsh

foreach ($vm in $args) {
    $vm = Get-VM $vm
    $vmhost = $vm.VMHost
    switch -wildcard ($vmhost.Name) {
        "sby-vhost*.srv.uis.private.cam.ac.uk" {
	    $clus = Get-Cluster SBY
	    $ds = Get-Datastore SB_SC_VM_vol6
	}
	"wcdc-vhost*.srv.uis.private.cam.ac.uk" {
	    $clus = Get-Cluster WCDC
	    $ds = Get-Datastore WC_UCS_SC_VM_vol5
	}
	"*-vhost22.srv.uis.private.cam.ac.uk" {
	    Move-VM $vm -Destination $clus -Datastore $ds
	}
	"*-vhost0*.srv.uis.private.cam.ac.uk" {
	    Move-VM $vm -Destination $clus
	}
    }
    if ($vm.GuestId -eq "sles11_64Guest") {
	$grp = Get-DrsClusterGroup -cluster $clus -name "SLES VMs"
	Set-DrsClusterGroup -vm $vm -add $grp
    }
    Start-VM $vm
}
