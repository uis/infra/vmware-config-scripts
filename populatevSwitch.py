#!/usr/bin/env python
"""
Written by Sam Wenham
Github: https://github.com/sdwenham
Email:sdw37@cam.ac.uk
This code has been released under the terms of the Apache-2.0 license
http://opensource.org/licenses/Apache-2.0
"""
"""from __future__ import print_function"""

import atexit
import sys
import argparse
import getpass
import re
import yaml

from pyVim.connect import SmartConnect, Disconnect
from pyVmomi import vim

try:
	from pyVim.connect import SmartConnectNoSSL
except ImportError:
	pass


def get_args():
    parser = argparse.ArgumentParser(
        description='Arguments for talking to vCenter')

    parser.add_argument('-s', '--host',
                        required=True,
                        action='store',
                        help='vSphere service to connect to')

    parser.add_argument('-o', '--port',
                        type=int,
                        default=443,
                        action='store',
                        help='Port to connect on')

    parser.add_argument('-u', '--user',
                        required=True,
                        action='store',
                        help='User name to use')

    parser.add_argument('-p', '--password',
                        required=True,
                        action=Password,
                        nargs='?',
                        dest='password',
                        help='Enter your password')

    parser.add_argument('-y', '--yaml',
                        required=False,
                        action='store',
                        help='YAML network config file to use')

    parser.add_argument('-v', '--vswitch',
                        #required=True,
                        action='store',
                        help='vSwitch')

    parser.add_argument('-c', '--skip_verification',
                        required=False,
                        action='store_true',
                        help='Skip SSL verification')

    parser.add_argument('-r', '--regex_esxi',
                        required=False,
                        default=None,
                        action='store',
                        help='Regex esxi name')

    parser.add_argument('-a', '--apply',
                        required=False,
                        default=None,
                        action='store_true',
                        help='Apply to host')

    parser.add_argument('-d', '--dump',
                        required=False,
                        default=None,
                        action='store',
                        help='dump config to file')

    parser.add_argument('-f', '--force',
                        required=False,
                        default=False,
                        action='store_true',
                        help='force apply even if not in maintenance')

    args = parser.parse_args()
    return args


class Password(argparse.Action):
    def __call__(self, parser, namespace, values, option_string):
        if values is None:
            values = getpass.getpass()

        setattr(namespace, self.dest, values)


def GetVMHosts(content, regex_esxi=None):
    host_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                        [vim.HostSystem],
                                                        True)
    obj = [host for host in host_view.view]
    match_obj = []
    if regex_esxi:
        for esxi in obj:
            if re.findall(r'%s.*' % regex_esxi, esxi.name):
                match_obj.append(esxi)
        match_obj_name = [match_esxi.name for match_esxi in match_obj]
        print("Matched ESXi hosts: %s" % match_obj_name)
        host_view.Destroy()
        return match_obj
    else:
        host_view.Destroy()
        return obj


def AddHostsPortgroup(hosts, vswitchName, portgroupName, vlanId):
    for host in hosts:
        AddHostPortgroup(host, vswitchName, portgroupName, vlanId)


def AddHostPortgroup(host, vswitchName, portgroupName, vlanId):
    portgroup_spec = vim.host.PortGroup.Specification()
    portgroup_spec.vswitchName = vswitchName
    portgroup_spec.name = portgroupName
    portgroup_spec.vlanId = int(vlanId)
    network_policy = vim.host.NetworkPolicy()
    network_policy.security = vim.host.NetworkPolicy.SecurityPolicy()
    #network_policy.security.allowPromiscuous = False
    #network_policy.security.macChanges = True
    #network_policy.security.forgedTransmits = True
    portgroup_spec.policy = network_policy

    host.configManager.networkSystem.AddPortGroup(portgroup_spec)


def applyyamlconfig(yamlconfig, hosts, force):
    #print(yamlconfig)
    #print(runningconfig)
    for host in hosts:
        print(host.name)
        if host.runtime.inMaintenanceMode is True or force is True:
            print(" - Host in Maintenance mode, applying config")
            for vswitch in yamlconfig:
                yamlvswname = vswitch["vSwitch"]["name"]
                for activevsw in host.configManager.networkSystem.networkInfo.vswitch:
                    if yamlvswname == activevsw.name:
                        print(activevsw.name)
                        print(vswitch["vSwitch"]["mtu"])
                        print(activevsw.mtu)
                        if vswitch["vSwitch"]["mtu"] != activevsw.mtu:
                            print("MTU mismatch, setting ...")
                        for activeportgroup in vswitch["vSwitch"]["portGroups"]:
                            #print(activeportgroup)
                            print(activeportgroup["vlan"], activeportgroup["vlanID"])
                            try:
                                AddHostPortgroup(host, yamlvswname, activeportgroup["vlan"], activeportgroup["vlanID"])
                            except vim.fault.AlreadyExists:
                                print(" - PortGroup already on host")
                        #print(host.configManager.networkSystem.networkInfo.vswitch)
        else:
            print(" - Not in Maintenance mode, can't apply config")


def dumpyamlconfig(yamlfile, config):
    if len(config) == 1:
        print("Writing yaml config to " + yamlfile)
        output = config[0]["host"]["vswitches"]
        stream = open(yamlfile, 'w')
        yaml.dump(output, stream)  # Write a YAML representation of data to file.
    else:
        print("We can only dump the config from a single host use -r to limit the hosts")
        #print(yaml.dump(config))  # Output the document to the screen.


def ReadYAMLnetworks(ymlFile):
    with open(ymlFile, 'r') as stream:
        try:
            vswitches = yaml.safe_load(stream)
            return vswitches

        except yaml.YAMLError as exc:
            print(exc)


def getHostsvSwitches(hosts):
    hostsSwitchesDict = {}
    for host in hosts:
        hostsSwitchesDict[host] = getHostvSwitches(host)
    return hostsSwitchesDict


def getHostvSwitches(host):
    switches = host.configManager.networkSystem.networkInfo.vswitch
    hostSwitches = switches
    return hostSwitches


def getvSwitchPortGroup(host, vswitch):
    switchportgroup = []
    for index, portgroup in enumerate(host.configManager.networkSystem.networkInfo.portgroup):
        if portgroup.spec.vswitchName == vswitch.name:
            switchportgroup.append({"name": portgroup.spec.name, "vlanID": portgroup.spec.vlanId})
    return switchportgroup


def main():
    args = get_args()

    if args.skip_verification:
        serviceInstance = SmartConnectNoSSL(host=args.host,
                                            user=args.user,
                                            pwd=args.password,
                                            port=443)
    else:
        serviceInstance = SmartConnect(host=args.host,
                                       user=args.user,
                                       pwd=args.password,
                                       port=443)
    atexit.register(Disconnect, serviceInstance)
    content = serviceInstance.RetrieveContent()

    #print(args)

    force = args.force

    if args.yaml:
        yamlvswitches = ReadYAMLnetworks(args.yaml)

        print("YAML file config")
        #print(vswitches)
        for switch in yamlvswitches:
            print(" - " + switch["vSwitch"]["name"])
            print("   mtu " + str(switch["vSwitch"]["mtu"]))
            for portgroup in switch["vSwitch"]["portGroups"]:
                print("    - " + portgroup["vlan"])
                print("      " + str(portgroup["vlanID"]))

    hosts = GetVMHosts(content, args.regex_esxi)
    hostvswitches = getHostsvSwitches(hosts)

    vswitchrunningconfig = []
    i = 0
    for host, vswitches in hostvswitches.items():

        print("Host:", host.name)

        vswitchrunningconfig.append({"host": {}})
        vswitchrunningconfig[i]["host"]["name"] = host.name
        vswitchrunningconfig[i]["host"]["vswitches"] = []

        for index, switch in enumerate(vswitches):

            newvswitch = {}
            newvswitch["name"] = switch.name
            newvswitch["mtu"] = switch.mtu

            portgroups = getvSwitchPortGroup(host, switch)
            print(" - " + switch.name)
            print("   mtu " + str(switch.mtu))
            newportgroups = []
            for portgroup in portgroups:
                print("    - " + portgroup["name"])
                print("      " + str(portgroup["vlanID"]))
                newportgroup = {}
                newportgroup["vlan"] = portgroup["name"]
                newportgroup["vlanID"] = portgroup["vlanID"]

                newportgroups.append(newportgroup)

            newvswitch["portGroups"] = newportgroups
            vswitchrunningconfig[i]["host"]["vswitches"].append({"vSwitch": newvswitch})
        i = i + 1

    #print(yamlvswitches)
    #print(vswitchrunningconfig[0]["host"]["vswitches"])

    if args.apply and args.yaml:
        applyyamlconfig(yamlvswitches, hosts, force)

    if args.dump:
        dumpyamlconfig(args.dump, vswitchrunningconfig)

# Main section
if __name__ == "__main__":
    sys.exit(main())
